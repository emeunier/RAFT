#!/bin/bash
#OAR -l {gpu_model NOT IN ('GeForce RTX 2080 Ti', 'Tesla K80', 'RTX A5000', 'A40', 'A100')}/gpu_device=1,walltime=48:00:00
#OAR -O /net/serpico-fs2/emeunier/Logs/raft_job.%jobid%.output
#OAR -E /net/serpico-fs2/emeunier/Logs/raft_job.%jobid%.error
set -xv

. /etc/profile.d/modules.sh

module load spack/singularity

cd $Codinria/RAFT
singularity exec -B /net:/net --nv $Dataria/Singularity/raft_latest.sif ./batch_raft.sh
