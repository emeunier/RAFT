import os, re
from pathlib import Path
from glob import glob
from argparse import Namespace
from demo import demo
import datetime
from ipdb import set_trace

net=f'{os.environ["Dataria"]}/Models/RAFT/models/raft-sintel.pth'
inp=f'{os.environ["Dataria"]}/video_sncf/JPEGImages/*'
gaps = [1]


model_name = re.match('^.*\/(.*).pth$', net)[1]

for gap in gaps :
    for in_data in glob(inp) :
        out_data = in_data.replace('JPEGImages', f'OpticalFlow/OpticalFlowRaft/{model_name}_gap{gap}/')
        print(f'Processing : {in_data} Output : {out_data}')
        if Path(out_data).exists() :
            print('Already processed.')
        else :
            params = {'model' : net, 'path' : in_data, 'save':True, 'path_save' : out_data, 'gap' : gap, 'small' : False, 'mixed_precision' : False, 'alternate_corr' : False}
            demo(Namespace(**params))
            with open(f'{out_data}/descrition.txt', 'w') as f:
                f.write(f'Flow computed using RAFT code on the {datetime.datetime.now()}\n\
                {params}')
