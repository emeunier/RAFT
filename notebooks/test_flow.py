import sys
sys.path.append('core')

import argparse
import os
import cv2
import glob
import numpy as np
import torch
from PIL import Image
from tqdm import tqdm

from raft import RAFT
from utils import flow_viz, frame_utils
from utils.utils import InputPadder
from pathlib import Path
from ipdb import set_trace
from natsort import natsorted
from argparse import Namespace

import flowiz
import matplotlib.pyplot as plt

DEVICE = 'cpu'

def load_image(imfile):
    img = np.array(Image.open(imfile)).astype(np.uint8)
    img = torch.from_numpy(img).permute(2, 0, 1).float()
    return img[None].to(DEVICE)


# Model :
mdp = f'{os.environ["Dataria"]}/Models/RAFT/models/raft-sintel.pth'
args = Namespace(model=mdp, small=False, mixed_precision=False, alternate_corr=False)
model = torch.nn.DataParallel(RAFT(args))
model.load_state_dict(torch.load(args.model, map_location=DEVICE))

model = model.module
model.to(DEVICE)
model.eval()


# Image :
imfile1 = f'{os.environ["Dataria"]}/SegTrackv2/JPEGImages/birdfall/birdfall2_00037.png'
imfile2 = f'{os.environ["Dataria"]}/SegTrackv2/JPEGImages/birdfall/birdfall2_00039.png'
image1 = load_image(imfile1)
image2 = load_image(imfile2)
padder = InputPadder(image1.shape)
image1, image2 = padder.pad(image1, image2)

# Compute :
flow_low, flow_up = model(image1, image2, iters=20, test_mode=True) # Flow Up is the upsampled version



plt.imshow(flowiz.convert_from_flow(flow_up[0].permute(1,2,0).detach().numpy()))
